package com.adamek

import com.adamek.rx.CoreSchedulers
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class TestSchedulers : CoreSchedulers {
    override fun main(): Scheduler = Schedulers.trampoline()

    override fun background(): Scheduler = Schedulers.trampoline()
}