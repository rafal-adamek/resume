package com.adamek.resume

import com.adamek.TestSchedulers
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Test

class ResumePresenterTest {

    private val stateMachine: ResumeStateMachine = ResumeStateMachine()

    @Test
    fun `assert default state`() {

        stateMachine.get().test()
            .assertNoErrors()
            .assertValue {
                it is ResumeState.Default
            }
    }

    @Test
    fun `assert success state`() {

        val presenter = create(
            stateMachine = stateMachine,
            resumeRepository = mockk {
                every { resume() } returns Single.just(mockk())
            })

        presenter.fetchResume()

        stateMachine.get().test()
            .assertNoErrors()
            .assertValue {
                it is ResumeState.Success
            }

    }

    @Test
    fun `assert error state`() {

        val presenter = create(
            stateMachine = stateMachine,
            resumeRepository = mockk {
                every { resume() } returns Single.error(Throwable())
            })

        presenter.fetchResume()

        stateMachine.get().test()
            .assertValue {
                it is ResumeState.Errror
            }
    }

    companion object {
        fun create(
            stateMachine: ResumeStateMachine,
            resumeRepository: ResumeRepository = mockk()
        ): ResumeContract.Presenter = ResumePresenter(
            TestSchedulers(),
            stateMachine,
            resumeRepository
        )
    }
}
