package com.adamek.resume

import org.junit.Test


class ResumeStateMachineTest {

    @Test
    fun `default state correct`() {
        val stateMachine: ResumeStateMachine = ResumeStateMachine()

        stateMachine.get().test()
            .assertNoErrors()
            .assertValue {
                it is ResumeState.Default
            }
    }

    @Test
    fun `progress state continues`() {
        val stateMachine: ResumeStateMachine = ResumeStateMachine()

        stateMachine.get().onNext(ResumeState.Progress)
        stateMachine.get().onNext(ResumeState.Errror(Throwable()))
        stateMachine.get().onNext(ResumeState.Progress)

        stateMachine.get().test()
            .assertNoErrors()
            .assertValue {
                it is ResumeState.Progress
            }
    }

}
