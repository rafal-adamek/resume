package com.adamek.resume

import com.adamek.base.StateMachine
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject

class ResumeStateMachine : StateMachine<ResumeState> {

    private val subject: Subject<ResumeState> = BehaviorSubject.createDefault(ResumeState.Default)

    override fun get(): Subject<ResumeState> = subject

    companion object {
        const val Name = "ResumeStateMachine"
    }
}