package com.adamek.resume

import com.adamek.base.StateMachine
import com.adamek.rx.CoreSchedulers
import io.reactivex.disposables.CompositeDisposable

class ResumePresenter(
    private val coreSchedulers: CoreSchedulers,
    private val stateMachine: StateMachine<@JvmSuppressWildcards ResumeState>,
    private val resumeRepository: ResumeRepository
) : ResumeContract.Presenter {

    private val disposables = CompositeDisposable()

    override fun fetchResume() {
        resumeRepository.resume()
            .subscribeOn(coreSchedulers.background())
            .observeOn(coreSchedulers.main())
            .doOnSubscribe { submit(ResumeState.Progress) }
            .subscribe(
                { submit(ResumeState.Success(it)) },
                { submit(ResumeState.Errror(it)) })
            .let(disposables::add)
    }

    private fun submit(state: ResumeState) = stateMachine.get().onNext(state)
}