package com.adamek.resume

interface ResumeContract {
    interface View {
        fun render()
    }

    interface Presenter {
        fun fetchResume()
    }
}