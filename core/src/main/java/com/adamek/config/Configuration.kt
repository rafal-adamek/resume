package com.adamek.config

interface Configuration {
    val email: String
    val url: String
}