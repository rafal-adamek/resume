package com.adamek.base

import io.reactivex.subjects.Subject

interface StateMachine<T : State> {
    fun get(): Subject<T>
}