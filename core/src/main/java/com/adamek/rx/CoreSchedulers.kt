package com.adamek.rx

import io.reactivex.Scheduler

interface CoreSchedulers {
    fun main(): Scheduler
    fun background(): Scheduler
}