package com.adamek.resume

import com.adamek.base.State
import com.adamek.resume.model.Resume

sealed class ResumeState : State {
    object Default : ResumeState()
    object Progress : ResumeState()
    data class Success(val resume: Resume) : ResumeState()
    data class Errror(val throwable: Throwable) : ResumeState()
}