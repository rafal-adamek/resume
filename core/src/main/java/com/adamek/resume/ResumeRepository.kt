package com.adamek.resume

import com.adamek.resume.model.Resume
import io.reactivex.Single

interface ResumeRepository {
    fun resume(): Single<Resume>
}