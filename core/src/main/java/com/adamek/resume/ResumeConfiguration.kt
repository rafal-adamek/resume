package com.adamek.resume

import com.adamek.config.Configuration

class ResumeConfiguration : Configuration {
    override val email: String = "rafal.adamek@hotmail.com"
    override val url: String = "https://gitlab.com/snippets/"

}