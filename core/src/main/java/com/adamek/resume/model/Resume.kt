package com.adamek.resume.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Resume(
    val name: String,
    val title: String,
    val email: String,
    val phone: String,
    val website: String,
    val skills: List<String>,
    val experience: List<Experience>,
    val projects: List<Project>
) {

    @JsonClass(generateAdapter = true)
    data class Experience(
        val title: String,
        val company: String,
        val from: String,
        val to: String,
        val entries: List<String>
    )

    @JsonClass(generateAdapter = true)
    data class Project(
        val title: String,
        val description: String
    )
}