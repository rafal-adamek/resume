package com.adamek.resume.api

import com.adamek.resume.model.Resume
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers

interface ResumeApi {

    @Headers("Accept: application/json")
    @GET("1924493/raw.json")
    fun resume(): Single<Resume>
}