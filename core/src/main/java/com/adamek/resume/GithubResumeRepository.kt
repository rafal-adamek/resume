package com.adamek.resume

import com.adamek.resume.api.ResumeApi
import com.adamek.resume.model.Resume
import io.reactivex.Single

class GithubResumeRepository(
    private val resumeApi: ResumeApi
) : ResumeRepository {
    override fun resume(): Single<Resume> = resumeApi.resume()
}