package com.adamek.resume;


import com.adamek.resume.api.ResumeApi
import org.junit.Test;
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class GithubResumeRepositoryTest {

    @Test
    fun `real api fetch`() {
        val repository = GithubResumeRepository(Retrofit.Builder()
            .baseUrl("https://gitlab.com/snippets/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(ResumeApi::class.java))

        repository.resume().test()
            .assertNoErrors()
            .assertComplete()

    }
}
