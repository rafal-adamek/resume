package com.adamek.view

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout

fun View.constraintMatchWrap(): ConstraintLayout.LayoutParams =
    ConstraintLayout.LayoutParams(
        ConstraintLayout.LayoutParams.MATCH_PARENT,
        ConstraintLayout.LayoutParams.WRAP_CONTENT
    )