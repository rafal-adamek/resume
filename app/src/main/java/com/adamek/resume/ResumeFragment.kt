package com.adamek.resume


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.adamek.base.StateMachine
import com.adamek.config.Configuration
import com.adamek.resume.model.Resume
import com.adamek.rx.CoreSchedulers
import com.adamek.view.constraintMatchWrap
import com.bumptech.glide.Glide
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_resume.*
import kotlinx.android.synthetic.main.fragment_resume.view.*
import javax.inject.Inject

class ResumeFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_resume, container, false)
        Glide.with(this.context)
            .load(
                resources
                    .getIdentifier("resume", "drawable", activity!!.packageName)
            )
            .into(view.toolbarImage)
        view.fab.setOnClickListener { sendEmail() }
        initStateMachine()

        return view
    }

    @Inject
    lateinit var configuration: Configuration

    @Inject
    lateinit var coreSchedulers: CoreSchedulers

    @Inject
    lateinit var stateMachine: StateMachine<@JvmSuppressWildcards ResumeState>

    @Inject
    lateinit var presenter: ResumeContract.Presenter

    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    private fun initStateMachine() {
        stateMachine.get()
            .subscribeOn(coreSchedulers.background())
            .observeOn(coreSchedulers.main())
            .subscribeBy(::handleError, ::handleStateComplete, ::handleState)
            .let(disposables::add)
    }

    private fun handleStateComplete() = Unit

    private fun handleState(state: ResumeState) {
        progress.visibility = if (state is ResumeState.Progress) View.VISIBLE else View.INVISIBLE

        when (state) {
            is ResumeState.Default -> presenter.fetchResume()
            is ResumeState.Success -> layout(state.resume)
            is ResumeState.Errror -> handleError(state.throwable)
        }
    }

    private fun handleError(throwable: Throwable) {
        Toast.makeText(this.context, throwable.message, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("SetTextI18n")
    private fun layout(resume: Resume) {
        inflate(R.layout.element_details).apply {
            layoutParams = constraintMatchWrap().apply { setMargins(8, 0, 8, 16) }

            findViewById<TextView>(R.id.name).text = resume.name
            findViewById<TextView>(R.id.title).text = resume.title
            findViewById<TextView>(R.id.website).text = resume.website
            findViewById<TextView>(R.id.phone).text = resume.phone
            findViewById<TextView>(R.id.email).text = resume.email

            view?.scrollContainer?.addView(this)
        }

        inflate(R.layout.element_title).apply {
            findViewById<TextView>(R.id.text).text = context.getString(R.string.title_experience)
            view?.scrollContainer?.addView(this)
        }

        for (experience in resume.experience) {
            inflate(R.layout.element_experience).apply {
                layoutParams = constraintMatchWrap().apply { setMargins(8, 0, 8, 0) }
                findViewById<TextView>(R.id.title).text = "${experience.title}, ${experience.company}"
                findViewById<TextView>(R.id.date).text = "${experience.from} - ${experience.to}"
                findViewById<TextView>(R.id.entries).apply {
                    text = experience.entries.joinToString("\n").also {
                        visibility = if (it.isBlank()) View.GONE else View.VISIBLE
                    }
                }

                view?.scrollContainer?.addView(this)
            }
        }

        inflate(R.layout.element_title).apply {
            findViewById<TextView>(R.id.text).text = getString(R.string.title_projects)
            view?.scrollContainer?.addView(this)
        }

        for (project in resume.projects) {
            inflate(R.layout.element_project).apply {
                layoutParams = constraintMatchWrap().apply { setMargins(8, 0, 8, 0) }
                findViewById<TextView>(R.id.title).text = project.title
                findViewById<TextView>(R.id.description).text = project.description

                view?.scrollContainer?.addView(this)
            }
        }
    }

    private fun inflate(@LayoutRes resId: Int) =
        LayoutInflater.from(this.context).inflate(resId, null, false)

    private fun sendEmail() =
        Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:${configuration.email}")).apply {
            putExtra(Intent.EXTRA_SUBJECT, getString(R.string.email_send_subject))
            putExtra(Intent.EXTRA_TEXT, getString(R.string.email_send_text))
            startActivity(Intent.createChooser(this, getString(R.string.email_send_title)))
        }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }

    companion object {
        fun newInstance() = ResumeFragment()
    }
}
