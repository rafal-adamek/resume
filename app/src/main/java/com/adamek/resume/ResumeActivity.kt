package com.adamek.resume

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class ResumeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resume)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ResumeFragment.newInstance())
            .commit()
    }

}
