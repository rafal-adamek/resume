package com.adamek.di

import android.content.Context
import com.adamek.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ClassBuilder::class,
        ConfigurationModule::class,
        StateModule::class,
        RxModule::class,
        NetworkModule::class
    ]
)
@Singleton
interface AppComponent {
    fun inject(app: App)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun appContext(app: Context): Builder

        fun build(): AppComponent
    }
}
