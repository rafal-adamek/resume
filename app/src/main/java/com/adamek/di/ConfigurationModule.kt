package com.adamek.di

import com.adamek.config.Configuration
import com.adamek.resume.ResumeConfiguration
import dagger.Module
import dagger.Provides

@Module
object ConfigurationModule {
    @Provides
    fun provideConfiguration(): Configuration = ResumeConfiguration()
}