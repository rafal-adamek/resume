package com.adamek.di.resume

import com.adamek.base.StateMachine
import com.adamek.resume.ResumeContract
import com.adamek.resume.ResumePresenter
import com.adamek.resume.ResumeRepository
import com.adamek.resume.ResumeState
import com.adamek.rx.CoreSchedulers
import dagger.Module
import dagger.Provides

@Module
object ResumeModule {
    @Provides
    fun providePresenter(
        coreSchedulers: CoreSchedulers,
        resumeRepository: ResumeRepository,
        stateMachine: StateMachine<@JvmSuppressWildcards ResumeState>
    ): ResumeContract.Presenter =
        ResumePresenter(coreSchedulers, stateMachine, resumeRepository)
}