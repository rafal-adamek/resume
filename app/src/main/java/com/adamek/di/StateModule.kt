package com.adamek.di

import com.adamek.base.StateMachine
import com.adamek.resume.ResumeState
import com.adamek.resume.ResumeStateMachine
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import dagger.multibindings.StringKey
import javax.inject.Singleton

@Module
object StateModule {

    @Provides
    @Singleton
    fun provideResumeStateMachine(): StateMachine<@JvmSuppressWildcards ResumeState> =
        ResumeStateMachine()

}