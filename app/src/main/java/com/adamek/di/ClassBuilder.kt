package com.adamek.di

import com.adamek.di.resume.ResumeModule
import com.adamek.resume.ResumeActivity
import com.adamek.resume.ResumeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ClassBuilder {

    @ContributesAndroidInjector(modules = [ResumeModule::class])
    abstract fun ResumeFragment(): ResumeFragment
}
