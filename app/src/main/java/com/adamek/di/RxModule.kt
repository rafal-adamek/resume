package com.adamek.di

import com.adamek.rx.AndroidSchedulers
import com.adamek.rx.CoreSchedulers
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
object RxModule {
    @Provides
    @Reusable
    fun provideRxSchedulers(): CoreSchedulers = AndroidSchedulers()
}