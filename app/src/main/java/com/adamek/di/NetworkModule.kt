package com.adamek.di

import com.adamek.config.Configuration
import com.adamek.resume.GithubResumeRepository
import com.adamek.resume.ResumeRepository
import com.adamek.resume.api.ResumeApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
object NetworkModule {

    @Provides
    fun provideResumeApi(configuration: Configuration): ResumeApi = Retrofit.Builder()
        .baseUrl(configuration.url)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
        .create(ResumeApi::class.java)

    @Provides
    fun provideResumeRepository(resumeApi: ResumeApi): ResumeRepository =
        GithubResumeRepository(resumeApi)
}