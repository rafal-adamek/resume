package com.adamek.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AndroidSchedulers : CoreSchedulers {
    override fun main(): Scheduler = AndroidSchedulers.mainThread()

    override fun background(): Scheduler = Schedulers.io()
}